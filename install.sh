#!/bin/bash

clear

sudo sed -i 's/^#ParallelDownloads = 5$/ParallelDownloads = 5/' /etc/pacman.conf

git clone https://aur.archlinux.org/yay-bin.git && cd yay-bin && makepkg -si

sudo pacman -S --needed --noconfirm bleachbit xorg-xhost cliphist xdg-desktop-portal-hyprland udiskie rofi thunar waybar kitty fastfetch dunst hyprland hyprpaper grim slurp pulseaudio ttf-jetbrains-mono-nerd noto-fonts-emoji ttf-liberation ttf-dejavu ttf-fira-sans ttf-fira-mono pamixer polkit-kde-agent qt5-wayland qt6-wayland imv gtk3 xdg-desktop-portal zip unzip qt5-graphicaleffects qt5-quickcontrols2 papirus-icon-theme noto-fonts-extra noto-fonts-cjk noto-fonts cmatrix

yay -S --needed --noconfirm cava waypaper-git hyprpicker bibata-cursor-theme wlogout pipes.sh

cp -R ~/dots/source/cava ~/.config/
cp -R ~/dots/source/dunst ~/.config/
cp -R ~/dots/source/fastfetch ~/.config/
cp -R ~/dots/source/gtk* ~/.config/
cp -R ~/dots/source/hypr ~/.config/
cp -R ~/dots/source/kitty ~/.config/
cp -R ~/dots/source/waypaper ~/.config/
cp -R ~/dots/source/waybar ~/.config/
cp -R ~/dots/source/wlogout ~/.config/
cp -R ~/dots/source/rofi ~/.config/
cp ~/dots/.bashrc ~/
sudo mv ~/dots/Tokyonight-Dark-B-LB /usr/share/themes/
mv ~/dots/Images ~/

sudo sed -i 's/^Inherits=Adwaita$/Inherits=Bibata-Modern-Ice/' /usr/share/icons/default/index.theme

clear

echo 'Installation complete.'